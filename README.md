---
marp: true
theme: ame
size: 4K
paginate: true
footer: '[![](/themes/Antoine_mini.png)](https://github.com/AntoineMeheut)[Antoine Meheut](https://github.com/AntoineMeheut)'
header: '[&#9635;](#1, " ")  &nbsp; Futur of IDE'
---

<!-- class: invert -->
# IDE Red Hat's Dev Spaces on Openshift with Gitlab for CI/CD
### Testing and presentation
Paris, November 2022

[![Open in Gitpod](https://www.eclipse.org/che/contribute.svg)](https://devspaces.apps.sandbox-m2.ll9k.p1.openshiftapps.com/#https://gitlab.com/cloud_ide/dev-spaces-demo?che-editor=che-incubator/che-code/insiders)
[![Slides](https://cdn-icons-png.flaticon.com/128/609/609001.png)](https://cloud_ide.gitlab.io/dev-spaces-demo/README.html)

---

## What is Dev Spaces?
* In a nutshell, Dev Spaces is a multi-tenant IDE environment that runs on your OpenShift Container Platform cluster.  It also comes with OpenShift Container Platform, so there’s no added subscription or license required to use it,
* It means that you have a workspace server that runs exactly the same no matter where you choose to run OpenShift.  Each developer has their own login with their own workspaces (that run as pods) in their own namespace on the cluster,
* These workspaces are full IDE instances that have your source, language servers, tools, a terminal, and important commands all baked right in,
* All you need to access and use these workspaces is a modern browser. No fancy developer laptop required, and no clunky virtual desktop!

---

## Usefull Dev Spaces resources
* To try Dev Spaces
   - https://workspaces.openshift.com/ to try DevSpaces for free
   - Understand DevFile:
        - https://devfile.io/ pour comprendre l'écosysteme du devfile
        - https://registry.devfile.io/viewer des exemples de devfile 

* Dev Spaces documentation:
    - https://access.redhat.com/documentation/en-us/red_hat_openshift_dev_spaces/3.2/html/administration_guide/preparing-the-installation#calculating-devspaces-resource-requirements
    - https://access.redhat.com/documentation/ko-kr/red_hat_openshift_dev_spaces/3.2/html/user_guide/user-onboarding#doc-wrapper
    - https://devspaces.apps.cluster-2wgqs.2wgqs.sandbox2049.opentlc.com/#https://github.com/gestrem/che-plugin-registry/tree/devfilev2?che-editor=che-incubator/che-code/insiders
    - https://github.com/devfile/developer-images

---

## Reminder of tested needs
- I am DevSecOps and I carry out part of my activity on an internal Openshift PaaS cloud,
- I also work on public clouds such as IaaS and PaaS,
- The tools I use for my work are mainly : #TODO#

---

## How does it work ?
- To get started the fastest you need to consider 3 things about your repo :
    - Add to your README the [launch link of Dev Spaces](https://access.redhat.com/documentation/ko-kr/red_hat_openshift_dev_spaces/3.2/html/user_guide/user-onboarding#basic-actions-you-can-perform-on-a-workspace) on your repo and the [icon that goes well](https://www.eclipse.org/che/contribute.svg),
    - Create your [devfile](https://devfile.io/) to specify and configure your IDE,
    - Create your [developer image](https://github.com/devfile/developer-images) to specify you Container image with your tools,
- You have in this repo an example of these two files : devfile.yaml and Dockerfile.

---

## How to use your own Dockerfile
- #TODO#

---

## Test results
- #TODO#

---
